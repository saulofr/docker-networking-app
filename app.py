from flask import Flask
import urllib

app = Flask(__name__)

@app.route('/')
def hello():
    url = 'http://flask-app:5000'
    req = urllib.request.Request(url)
    res = urllib.request.urlopen(req).read()
    return res

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)